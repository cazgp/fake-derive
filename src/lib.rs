extern crate proc_macro;
extern crate quote;
extern crate syn;

use crate::proc_macro::TokenStream;
use quote::quote;

fn lit_to_usize(lit: &syn::Lit) -> Option<usize> {
    match *lit {
        syn::Lit::Int(ref s) => Some(s.value() as usize),
        syn::Lit::Str(ref s) => Some(s.value().parse::<usize>().unwrap()),
        _ => None,
    }
}

fn lit_to_string(lit: &syn::Lit) -> Option<String> {
    match *lit {
        syn::Lit::Str(ref s) => Some(s.value()),
        _ => None,
    }
}

fn quote_bool(
    namespace: &str,
    faker: &str,
    field_name: &proc_macro2::Ident,
) -> proc_macro2::TokenStream {
    let namespace = proc_macro2::Ident::new(&namespace, proc_macro2::Span::call_site());
    let faker = proc_macro2::Ident::new(&faker, proc_macro2::Span::call_site());
    quote! { #field_name: fake::fake!(#namespace.#faker) }
}

fn quote_string(
    namespace: &str,
    faker: &str,
    field_name: &proc_macro2::Ident,
) -> proc_macro2::TokenStream {
    let namespace = proc_macro2::Ident::new(&namespace, proc_macro2::Span::call_site());
    let faker = proc_macro2::Ident::new(&faker, proc_macro2::Span::call_site());

    quote! { #field_name: fake::fake!(#namespace.#faker).to_string() }
}

fn quote_maybe_unique_string(
    namespace: &str,
    faker: &str,
    field_name: &proc_macro2::Ident,
    is_unique: bool,
) -> proc_macro2::TokenStream {
    let namespace = proc_macro2::Ident::new(&namespace, proc_macro2::Span::call_site());
    let faker = proc_macro2::Ident::new(&faker, proc_macro2::Span::call_site());

    if is_unique {
        quote! {
            #field_name: format!("{} {}", fake::fake!(#namespace.#faker).to_string(), unique_string())
        }
    } else {
        quote! { #field_name: fake::fake!(#namespace.#faker).to_string() }
    }
}

fn quote_usize(
    namespace: &str,
    faker: &str,
    field_name: &proc_macro2::Ident,
) -> proc_macro2::TokenStream {
    let namespace = proc_macro2::Ident::new(&namespace, proc_macro2::Span::call_site());
    let faker = proc_macro2::Ident::new(&faker, proc_macro2::Span::call_site());
    quote! { #field_name: fake::fake!(#namespace.#faker).parse::<usize>().unwrap() }
}

fn field_with_one_attribute(
    field_name: proc_macro2::Ident,
    fake_type: &str,
    attribute_name: &str,
    tokens: &syn::punctuated::Punctuated<syn::NestedMeta, syn::token::Comma>,
) -> proc_macro2::TokenStream {
    if tokens.len() != 1 {
        panic!("Wrong arguments supplied to `{}`", field_name);
    }

    if let syn::NestedMeta::Meta(syn::Meta::NameValue(syn::MetaNameValue {
        ref ident,
        ref lit,
        ..
    })) = tokens[0]
    {
        match ident.to_string() {
            ref a if a == attribute_name => {
                let value = match lit_to_usize(lit) {
                    Some(f) => f,
                    None => panic!(
                        "Invalid argument type for `{}`: only a usize is allowed",
                        attribute_name
                    ),
                };
                let fake_type = proc_macro2::Ident::new(&fake_type, proc_macro2::Span::call_site());
                quote! { #field_name: fake::fake!(Lorem.#fake_type(#value)).iter().map(|s| s.to_string()).collect() }
            }
            x => panic!(
                "Invalid argument {} for `Lorem.{}`: only `{}` is allowed",
                x, field_name, attribute_name
            ),
        }
    } else {
        panic!("Invalid arguments to `{}`", field_name)
    }
}

fn field_with_count_and_max_extra_count(
    field_name: syn::Ident,
    fake_type: &str,
    tokens: &syn::punctuated::Punctuated<syn::NestedMeta, syn::token::Comma>,
) -> proc_macro2::TokenStream {
    if tokens.len() != 2 {
        panic!("Wrong arguments supplied to `{}`", field_name);
    }

    let mut count = None;
    let mut max_extra_count = None;

    for item in tokens {
        if let syn::NestedMeta::Meta(syn::Meta::NameValue(syn::MetaNameValue {
            ref ident,
            ref lit,
            ..
        })) = item
        {
            match ident.to_string().as_ref() {
                "count" => {
                    count = match lit_to_usize(lit) {
                        Some(f) => Some(f),
                        None => panic!("Invalid argument type for `count`: only a usize is allowed")
                    };
                },
                "max_extra_count" => {
                    max_extra_count = match lit_to_usize(lit) {
                        Some(f) => Some(f),
                        None => panic!("Invalid argument type for `max_extra_count`: only a usize is allowed")
                    };
                },
                x => panic!("Invalid argument {} for `Lorem.{}`: only `count` and `max_extra_count` are allowed", x, field_name)
            }
        } else {
            panic!("Invalid arguments to `{}`", field_name)
        }
    }

    let fake_type = proc_macro2::Ident::new(&fake_type, proc_macro2::Span::call_site());
    quote! { #field_name: fake::fake!(Lorem.#fake_type(#count, #max_extra_count)) }
}

fn fake_address(
    field_name: proc_macro2::Ident,
    tokens: &syn::NestedMeta,
) -> proc_macro2::TokenStream {
    match tokens {
        syn::NestedMeta::Meta(ref item) => match *item {
            syn::Meta::Word(ref name) => match name.to_string().as_ref() {
                x @ "building_number" => quote_string("Address", x, &field_name),
                x @ "city" => quote_string("Address", x, &field_name),
                x @ "city_prefix" => quote_string("Address", x, &field_name),
                x @ "city_suffix" => quote_string("Address", x, &field_name),
                x @ "latitude" => quote_string("Address", x, &field_name),
                x @ "longitude" => quote_string("Address", x, &field_name),
                x @ "postcode" => quote_string("Address", x, &field_name),
                x @ "secondary_address" => quote_string("Address", x, &field_name),
                x @ "state" => quote_string("Address", x, &field_name),
                x @ "state_abbr" => quote_string("Address", x, &field_name),
                x @ "street_address" => quote_string("Address", x, &field_name),
                x @ "street_name" => quote_string("Address", x, &field_name),
                x @ "street_suffix" => quote_string("Address", x, &field_name),
                x @ "time_zone" => quote_string("Address", x, &field_name),
                x @ "zip" => quote_string("Address", x, &field_name),
                x => panic!("Address.{} does not exist", x),
            },
            _ => panic!("attribute must be a word"),
        },
        _ => unimplemented!("Name non-meta"),
    }
}

fn fake_boolean(
    field_name: proc_macro2::Ident,
    tokens: &syn::NestedMeta,
) -> proc_macro2::TokenStream {
    match tokens {
        syn::NestedMeta::Meta(ref item) => match *item {
            syn::Meta::Word(ref name) => match name.to_string().as_ref() {
                x @ "boolean" => quote_bool("Boolean", x, &field_name),
                x => panic!("Boolean.{} does not exist", x),
            },
            _ => unimplemented!("Boolean non word"),
        },
        _ => unimplemented!("Boolean non-meta"),
    }
}

fn fake_internet(
    field_name: proc_macro2::Ident,
    tokens: &syn::NestedMeta,
) -> proc_macro2::TokenStream {
    match tokens {
        syn::NestedMeta::Meta(ref item) => match *item {
            syn::Meta::Word(ref name) => match name.to_string().as_ref() {
                x @ "color" => quote_string("Internet", x, &field_name),
                x @ "domain_suffix" => quote_string("Internet", x, &field_name),
                x @ "free_email" => quote_string("Internet", x, &field_name),
                x @ "free_email_provider" => quote_string("Internet", x, &field_name),
                x @ "ip" => quote_string("Internet", x, &field_name),
                x @ "ipv4" => quote_string("Internet", x, &field_name),
                x @ "ipv6" => quote_string("Internet", x, &field_name),
                x @ "safe_email" => quote_string("Internet", x, &field_name),
                x @ "user_agent" => quote_string("Internet", x, &field_name),
                x @ "user_name" => quote_string("Internet", x, &field_name),
                x => panic!("Internet.{} does not exist", x),
            },
            _ => panic!("attribute must be a word"),
        },
        _ => unimplemented!("Name non-meta"),
    }
}

fn fake_lorem(
    field_name: proc_macro2::Ident,
    tokens: &syn::NestedMeta,
) -> proc_macro2::TokenStream {
    match tokens {
        syn::NestedMeta::Meta(ref item) => match *item {
            syn::Meta::List(syn::MetaList {
                ref ident,
                ref nested,
                ..
            }) => match ident.to_string().as_ref() {
                // #[fake(Lorem(words(count = 10)))]
                x @ "words" => field_with_one_attribute(field_name, x, "count", nested),
                // #[fake(Lorem(sentence(count = 4, max_extra_count = 6)))]
                x @ "sentence" => field_with_count_and_max_extra_count(field_name, x, nested),
                // #[fake(Lorem(sentences(count = 4)))]
                x @ "sentences" => field_with_one_attribute(field_name, x, "count", nested),
                // #[fake(Lorem(paragraph(count = 7, max_extra_count = 3)))]
                x @ "paragraph" => field_with_count_and_max_extra_count(field_name, x, nested),
                // #[fake(Lorem(paragraphs(count = 3)))]
                x @ "paragraphs" => field_with_one_attribute(field_name, x, "count", nested),
                x => {
                    panic!("Lorem.{}(..) does not exist", x);
                }
            },
            syn::Meta::Word(ref name) => match name.to_string().as_ref() {
                x @ "word" => quote_string("Lorem", x, &field_name),
                x => panic!("Lorem.{} does not exist", x),
            },
            _ => unimplemented!("Lorem non list or word"),
        },
        _ => unimplemented!("Lorem non-meta"),
    }
}

fn fake_name(
    field_name: proc_macro2::Ident,
    tokens: &syn::NestedMeta,
    is_unique: bool,
) -> proc_macro2::TokenStream {
    match tokens {
        syn::NestedMeta::Meta(ref item) => match *item {
            syn::Meta::Word(ref name) => match name.to_string().as_ref() {
                x @ "first_name" => quote_maybe_unique_string("Name", x, &field_name, is_unique),
                x @ "last_name" => quote_maybe_unique_string("Name", x, &field_name, is_unique),
                x @ "name" => quote_maybe_unique_string("Name", x, &field_name, is_unique),
                x @ "name_with_middle" => {
                    quote_maybe_unique_string("Name", x, &field_name, is_unique)
                }
                x @ "title_descriptor" => {
                    quote_maybe_unique_string("Name", x, &field_name, is_unique)
                }
                x @ "title_level" => quote_maybe_unique_string("Name", x, &field_name, is_unique),
                x @ "title_job" => quote_maybe_unique_string("Name", x, &field_name, is_unique),
                x @ "title" => quote_maybe_unique_string("Name", x, &field_name, is_unique),
                x => panic!("Name.{} does not exist", x),
            },
            _ => panic!("attribute must be a word"),
        },
        _ => unimplemented!("Name non-meta"),
    }
}

fn fake_number(
    field_name: proc_macro2::Ident,
    tokens: &syn::NestedMeta,
) -> proc_macro2::TokenStream {
    match tokens {
        syn::NestedMeta::Meta(ref item) => match *item {
            syn::Meta::Word(ref name) => match name.to_string().as_ref() {
                x @ "digit" => quote_usize("Number", x, &field_name),
                x => panic!("Number.{} does not exist", x),
            },
            _ => panic!("attribute must be a word"),
        },
        _ => unimplemented!("Number non-meta"),
    }
}

fn fake_phone_number(
    field_name: proc_macro2::Ident,
    tokens: &syn::NestedMeta,
) -> proc_macro2::TokenStream {
    match tokens {
        syn::NestedMeta::Meta(ref item) => match *item {
            syn::Meta::NameValue(syn::MetaNameValue {
                ref ident, ref lit, ..
            }) => match ident.to_string().as_ref() {
                "phone_number_with_format" => match lit_to_string(lit) {
                    Some(format) => {
                        quote! { #field_name: fake::fake!(PhoneNumber.phone_number_with_format(#format)) }
                    }
                    None => {
                        panic!("invalid argument for `phone_number_with_format` validator: only strings are allowed")
                    }
                },
                x => panic!("invalid fake attribute {}", x),
            },
            syn::Meta::Word(ref name) => match name.to_string().as_ref() {
                x @ "cell_number" => quote_string("PhoneNumber", x, &field_name),
                x @ "phone_number" => quote_string("PhoneNumber", x, &field_name),
                x => panic!("PhoneNumber.{} does not exist", x),
            },
            ref x => {
                dbg!(x);
                panic!("attribute must be a word");
            }
        },
        _ => unimplemented!("Name non-meta"),
    }
}

fn fake_field(field: &syn::Field) -> proc_macro2::TokenStream {
    // Get attributes `#[..]` on each field
    for attr in field.attrs.iter() {
        if attr.path != syn::parse_quote!(fake) {
            continue;
        }

        let field_ident = field.ident.clone().unwrap();

        match attr.interpret_meta() {
            Some(syn::Meta::List(syn::MetaList { ref nested, .. })) => {
                // TODO locale should be an extra attribute but I don't need that now.
                if nested.len() != 1 {
                    panic!("must define only one fake attribute per field");
                }

                match nested[0] {
                    syn::NestedMeta::Meta(ref item) => match *item {
                        syn::Meta::List(syn::MetaList {
                            ref ident,
                            ref nested,
                            ..
                        }) => {
                            match ident.to_string().as_ref() {
                                x @ "Address" => {
                                    assert!(nested.len() == 1, "{} can only have one attribute", x);
                                    return fake_address(field_ident.clone(), &nested[0]);
                                },
                                x @ "Boolean" => {
                                    assert!(nested.len() == 1, "{} can only have one attribute", x);
                                    return fake_boolean(field_ident.clone(), &nested[0]);
                                }
                                x @ "Internet" => {
                                    assert!(nested.len() == 1, "{} can only have one attribute", x);
                                    return fake_internet(field_ident.clone(), &nested[0]);
                                }
                                x @ "Lorem" => {
                                    assert!(nested.len() == 1, "{} can only have one attribute", x);
                                    return fake_lorem(field_ident.clone(), &nested[0]);
                                }
                                x @ "Name" => {
                                    assert!(nested.len() < 3, "{} can have at most two attributes", x);

                                    // Check if the "unique" attribute is on name
                                    let is_unique = if nested.len() == 2 {
                                        match nested[1] {
                                            syn::NestedMeta::Meta(ref item) => match *item {
                                                syn::Meta::Word(ref name) => match name.to_string().as_ref() {
                                                    "unique" => true,
                                                    x => panic!("Name.{} does not exist", x),
                                                },
                                                _ => panic!("attribute must be a word"),
                                            },
                                            _ => unimplemented!("Name non-meta"),
                                        }
                                    } else {
                                        false
                                    };

                                    return fake_name(field_ident.clone(), &nested[0], is_unique);
                                }
                                x @ "Number" => {
                                    assert!(nested.len() == 1, "{} can only have one attribute", x);
                                    return fake_number(field_ident.clone(), &nested[0]);
                                }
                                x @ "PhoneNumber" => {
                                    assert!(nested.len() == 1, "{} can only have one attribute", x);
                                    return fake_phone_number(field_ident.clone(), &nested[0]);
                                }
                                x => panic!("fake type {} does not exist", x),
                            }
                        }
                        syn::Meta::NameValue(syn::MetaNameValue {
                            ref ident, ref lit, ..
                        }) => match ident.to_string().as_ref() {
                            "custom" => {
                                match lit_to_string(lit) {
                                    Some(fn_name) => {
                                        let fn_name = proc_macro2::Ident::new(&fn_name, proc_macro2::Span::call_site());
                                        let field_name = field_ident.clone();
                                        return quote! { #field_name: #fn_name() };
                                    }
                                    None => panic!("invalid argument for `custom` validator: only strings are allowed"),
                                }
                            }
                            x => panic!("invalid fake attribute {}", x),
                        },
                        ref x => {
                            dbg!(x);
                            panic!("invalid namespace")
                        }
                    },
                    _ => unimplemented!("non-meta"),
                }
            }
            _ => panic!("attributes are syntactically invalid"),
        }
    }

    // If we get here, panic because the user has defined things wrong
    panic!(
        "field {} must have 'fake' attribute",
        field.ident.clone().unwrap()
    );
}

fn impl_fake(ast: &syn::DeriveInput) -> proc_macro2::TokenStream {
    match ast.data {
        syn::Data::Struct(ref data_struct) => {
            match data_struct.fields {
                // Structure with named fields (as opposed to tuple-like struct or unit struct)
                syn::Fields::Named(ref fields_named) => {
                    // Iterate over the fields: `x`, `y`, ..
                    let fields: Vec<proc_macro2::TokenStream> = fields_named
                        .named
                        .iter()
                        .map(|field| {
                            // Generate the code for the field
                            fake_field(field)
                        })
                        .collect();

                    let name = &ast.ident;
                    let generics = &ast.generics;
                    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();
                    let result = quote! {
                        impl #impl_generics #name #ty_generics #where_clause {
                            fn fake() -> #name {
                                #name {
                                    #(#fields,)*
                                }
                            }
                        }
                    };
                    result
                }
                _ => panic!("can only have named fields"),
            }
        }
        _ => panic!("must be a struct"),
    }
}

#[proc_macro_derive(Fake, attributes(fake))]
pub fn fake_macro_derive(input: TokenStream) -> TokenStream {
    // Construct a representation of Rust code as a syntax tree
    // that we can manipulate
    let ast = syn::parse(input).unwrap();

    // Build the trait implementation
    impl_fake(&ast).into()
}
