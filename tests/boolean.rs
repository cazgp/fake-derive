extern crate fake;
extern crate fake_derive;
use fake_derive::Fake;

#[derive(Debug, Fake)]
pub struct Boolean {
    #[fake(Boolean(boolean))]
    boolean_field: bool,
}

#[test]
fn test_boolean() {
    // There's not much that can be asserted here...
    Boolean::fake();
}
