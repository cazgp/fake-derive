extern crate fake;
extern crate fake_derive;
use fake_derive::Fake;

#[derive(Debug, Fake)]
pub struct Internet {
    #[fake(Internet(color))]
    color_field: String,
    #[fake(Internet(domain_suffix))]
    domain_suffix_field: String,
    #[fake(Internet(free_email))]
    free_email_field: String,
    #[fake(Internet(free_email_provider))]
    free_email_provider_field: String,
    #[fake(Internet(ip))]
    ip_field: String,
    #[fake(Internet(ipv4))]
    ipv4_field: String,
    #[fake(Internet(ipv6))]
    ipv6_field: String,
    #[fake(Internet(safe_email))]
    safe_email_field: String,
    #[fake(Internet(user_agent))]
    user_agent_field: String,
    #[fake(Internet(user_name))]
    user_name_field: String,
}

#[test]
fn test_internet() {
    // There's not much that can be asserted here...
    Internet::fake();
}
