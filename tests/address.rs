extern crate fake;
extern crate fake_derive;
use fake_derive::Fake;

#[derive(Debug, Fake)]
pub struct Address {
    #[fake(Address(building_number))]
    building_number_field: String,
    #[fake(Address(city))]
    city_field: String,
    #[fake(Address(city_prefix))]
    city_prefix_field: String,
    #[fake(Address(city_suffix))]
    city_suffix_field: String,
    #[fake(Address(latitude))]
    latitude_field: String,
    #[fake(Address(longitude))]
    longitude_field: String,
    #[fake(Address(postcode))]
    postcode_field: String,
    #[fake(Address(secondary_address))]
    secondary_address_field: String,
    #[fake(Address(state))]
    state_field: String,
    #[fake(Address(state_abbr))]
    state_abbr_field: String,
    #[fake(Address(street_address))]
    street_address_field: String,
    #[fake(Address(street_name))]
    street_name_field: String,
    #[fake(Address(street_suffix))]
    street_suffix_field: String,
    #[fake(Address(time_zone))]
    time_zone_field: String,
    #[fake(Address(zip))]
    zip_field: String,
}

#[test]
fn test_address() {
    // There's not much that can be asserted here...
    Address::fake();
}
