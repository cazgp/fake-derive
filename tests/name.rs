extern crate fake;
extern crate fake_derive;
use fake_derive::Fake;
use nanoid;

fn unique_string() -> String {
    let alphabet: [char; 26] = [
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
        's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
    ];
    nanoid::custom(15, &alphabet)
}

#[derive(Debug, Fake)]
pub struct Name {
    #[fake(Name(first_name))]
    first_name_field: String,
    #[fake(Name(last_name))]
    last_name_field: String,
    #[fake(Name(name))]
    name_field: String,
    #[fake(Name(name_with_middle))]
    name_with_middle_field: String,
    #[fake(Name(title_descriptor))]
    title_descriptor_field: String,
    #[fake(Name(title_level))]
    title_level_field: String,
    #[fake(Name(title_job))]
    title_job_field: String,
    #[fake(Name(title))]
    title_field: String,
}

#[test]
fn test_name() {
    // There's not much that can be asserted here...
    Name::fake();
}

#[derive(Debug, Fake)]
pub struct NameUnique {
    #[fake(Name(first_name, unique))]
    first_name_field: String,
    #[fake(Name(last_name, unique))]
    last_name_field: String,
    #[fake(Name(name, unique))]
    name_field: String,
    #[fake(Name(name_with_middle, unique))]
    name_with_middle_field: String,
    #[fake(Name(title_descriptor, unique))]
    title_descriptor_field: String,
    #[fake(Name(title_level, unique))]
    title_level_field: String,
    #[fake(Name(title_job, unique))]
    title_job_field: String,
    #[fake(Name(title, unique))]
    title_field: String,
}

#[test]
fn test_name_unique() {
    // There's not much that can be asserted here...
    dbg!(NameUnique::fake());
    dbg!(NameUnique::fake());
}
