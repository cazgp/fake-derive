extern crate fake_derive;

#[derive(fake_derive::Fake)]
//~^ ERROR: proc-macro derive panicked
//~^^ HELP: 3:10: 3:27: message: attribute must be a word
pub struct Name {
    #[fake(Name(Moo(1)))]
    test: String,
}

fn main() {}
