extern crate fake_derive;

#[derive(fake_derive::Fake)]
//~^ ERROR: proc-macro derive panicked
//~^^ HELP: 3:10: 3:27: message: Name.not_unique does not exist
pub struct Name {
    #[fake(Name(title_job, not_unique))]
    test: String,
}

fn main() {}
