extern crate fake_derive;

#[derive(fake_derive::Fake)]
//~^ ERROR: proc-macro derive panicked
//~^^ HELP: 3:10: 3:27: message: must define only one fake attribute per field
pub struct Lorem {
    #[fake(Lorem(word), Lorem(word))]
    test: String,
}

fn main() {}
