extern crate fake_derive;

#[derive(fake_derive::Fake)]
//~^ ERROR: proc-macro derive panicked
//~^^ HELP: 3:10: 3:27: message: Name can have at most two attributes
pub struct Name {
    #[fake(Name(word, word, word))]
    test: String,
}

fn main() {}
