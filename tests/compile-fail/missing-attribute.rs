extern crate fake_derive;

#[derive(fake_derive::Fake)]
//~^ ERROR: proc-macro derive panicked
//~^^ HELP: 3:10: 3:27: message: field test must have 'fake' attribute
pub struct Lorem {
    test: String,
}

fn main() {}
