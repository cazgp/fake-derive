extern crate fake_derive;

#[derive(fake_derive::Fake)]
//~^ ERROR: proc-macro derive panicked
//~^^ HELP: 3:10: 3:27: message: invalid namespace
pub struct Lorem {
    #[fake(word)]
    word: String,
}

fn main() {}
