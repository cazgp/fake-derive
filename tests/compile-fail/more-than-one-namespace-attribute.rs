extern crate fake_derive;

#[derive(fake_derive::Fake)]
//~^ ERROR: proc-macro derive panicked
//~^^ HELP: 3:10: 3:27: message: Lorem can only have one attribute
pub struct Name {
    #[fake(Lorem(word, word))]
    test: String,
}

fn main() {}
