extern crate fake_derive;

#[derive(fake_derive::Fake)]
//~^ ERROR: proc-macro derive panicked
//~^^ HELP: 3:10: 3:27: message: can only have named fields
struct Test(String);

fn main() {}
