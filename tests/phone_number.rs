extern crate fake;
extern crate fake_derive;
use fake_derive::Fake;

#[derive(Debug, Fake)]
pub struct PhoneNumber {
    #[fake(PhoneNumber(cell_number))]
    cell_number_field: String,
    #[fake(PhoneNumber(phone_number))]
    phone_number_field: String,
    #[fake(PhoneNumber(phone_number_with_format = "########"))]
    phone_number_with_format_field: String,
}

#[test]
fn test_phone_number() {
    // There's not much that can be asserted here...
    let res = PhoneNumber::fake();
    assert_eq!(res.phone_number_with_format_field.len(), 8);
}
