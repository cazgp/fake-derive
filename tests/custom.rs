extern crate fake;
extern crate fake_derive;
use fake_derive::Fake;

fn custom_string() -> String {
    "moo".to_string()
}

fn custom_number() -> usize {
    1
}

#[derive(Debug, Eq, Fake, PartialEq)]
pub struct Custom {
    #[fake(custom = "custom_string")]
    custom_string_field: String,
    #[fake(custom = "custom_number")]
    custom_number_field: usize,
}

#[test]
fn test_custom() {
    let result = Custom::fake();
    assert_eq!(
        result,
        Custom {
            custom_string_field: "moo".to_string(),
            custom_number_field: 1
        }
    );
}
