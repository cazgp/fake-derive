extern crate fake;
extern crate fake_derive;
use fake_derive::Fake;

#[derive(Debug, Fake)]
pub struct Lorem {
    #[fake(Lorem(word))]
    word_field: String,
    #[fake(Lorem(words(count = 10)))]
    words_field: Vec<String>,
    #[fake(Lorem(sentence(count = 4, max_extra_count = 6)))]
    sentence_field: String,
    #[fake(Lorem(sentences(count = 4)))]
    sentences_field: Vec<String>,
    #[fake(Lorem(paragraph(count = 7, max_extra_count = 3)))]
    paragraph_field: String,
    #[fake(Lorem(paragraphs(count = 3)))]
    paragraphs_field: Vec<String>,
}

#[test]
fn test_lorem() {
    let result = Lorem::fake();
    assert_eq!(result.words_field.len(), 10);
    assert_eq!(result.sentences_field.len(), 4);
    assert_eq!(result.paragraphs_field.len(), 3);
}
