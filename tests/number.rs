extern crate fake;
extern crate fake_derive;
use fake_derive::Fake;

// TODO
#[derive(Debug, Fake)]
pub struct Number {
    // println!("{:?}", fake!(Number.number(10)));
    // println!("{:?}", fake!(Number.between(5, 10)));
    // println!("{:?}", fake!(Number.between(5.0_f32, 10.0_f32)));
    #[fake(Number(digit))]
    digit_field: usize,
}

#[test]
fn test_number() {
    // There's not much that can be asserted here...
    Number::fake();
}
